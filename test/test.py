import os
import subprocess

docker_image = 'bitbucketpipelines/demo-pipe-python:ci' + os.getenv('BITBUCKET_BUILD_NUMBER', 'local')

def docker_build():
  """
  Build the docker image for tests.
  :return:
  """
  args = [
    'docker',
    'build',
    '-t',
    docker_image,
    '.',
  ]
  subprocess.run(args, check=True)


def setup_module():
  docker_build()

def test_no_parameters():
  args = [
    'docker',
    'run',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, capture_output=True)
  assert result.returncode == 1
  assert 'API_KEY variable missing.' in result.stdout

def test_no_integration_key():
  args = [
    'docker',
    'run',
    '-e', 'API_KEY="foo"',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, capture_output=True)
  assert result.returncode == 1
  assert 'INTEGRATION_KEY variable missing.' in result.stdout


def test_success():
  args = [
    'docker',
    'run',
    '-e', f'API_KEY={os.getenv("API_KEY")}',
    '-e', f'INTEGRATION_KEY={os.getenv("INTEGRATION_KEY")}',
    '-e', f'DESCRIPTION=Test incident',
    '-e', f'INCIDENT_KEY',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, capture_output=True)
  assert result.returncode == 0
  assert 'Incident successfully created' in result.stdout


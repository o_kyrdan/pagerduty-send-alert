# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0

- minor: Fix passing of client_url and source inside incident details.
- minor: Removed unused pipe variables from the README.md

## 0.1.2

- patch: Fixed colourised logging.

## 0.1.1

- patch: Fixed missing variables

## 0.1.0

- minor: Initial release
- minor: Initial release of the pipe


import json
import requests
import string

from common import *

def is_hex(value):
  filtered = list(filter(lambda char: char in string.hexdigits, value))
  return len(filtered) == 32

def validate_integration_key(integration_key):
  if not is_hex(integration_key):
    raise Exception('INTEGRATION_KEY must be a 32 character hex value.')

def create_incident():
    try:
      api_key = get_variable('API_KEY', required=True)
      integration_key=get_variable('INTEGRATION_KEY', required=True)

      account = get_variable('BITBUCKET_REPO_OWNER', default='local')
      repo = get_variable('BITBUCKET_REPO_SLUG', default='local')
      build = get_variable('BITBUCKET_BUILD_NUMBER', default='local')

      incident_key = get_variable(
        'INCIDENT_KEY',
        default=f'{account}/{repo}-event')

      event_type = get_variable('EVET_TYPE', default='trigger')
      severity = get_variable('SEVERITY', default='error')
      description = get_variable('DESCRIPTION', default=f'Event triggered from Bitbucket Pipelines')
      client = get_variable('CLIENT', default='Bitbucket Pipelines')

      pipelines_url = f'https://bitbucket.org/{account}/{repo}/addon/pipelines/home#!/results/{build}'
      client_url = get_variable('CLIENT_URL', default=pipelines_url)

    except Exception as e:
      fail(message=str(e))



    header = {
        'Content-Type': 'application/json',
        'Authorization': f'Token token={api_key}'
    }

    payload = {
        "routing_key": integration_key,
        "event_action": event_type,
        "dedup_key": incident_key,
        "client": client,
        "client_url": client_url,
        "payload": {
            "summary": description,
            "source": pipelines_url,
            "severity": severity,
        }
    }

    logger.info('Sending and event to PageDuty...')

    response = requests.post('https://events.pagerduty.com/v2/enqueue',
                            data=json.dumps(payload),
                            headers=header)

    if response.status_code != 202:
        fail(message=f'Failed to send an event to pagerduty: {response.text}')

    success('Incident successfully created.')

if __name__=='__main__':
    logger = configure_logger()
    enable_debug()
    create_incident()
